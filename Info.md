A Comprehensive Introduction to Anomaly Detection https://www.datacamp.com/tutorial/introduction-to-anomaly-detection

Anomaly Detection Methods And When to Use Each One
Anomaly detection algorithms differ depending on the type of outliers and the structure in the dataset.
For univariate outlier detection, the most popular methods are:

    Z-score (standard score): the z-score measures how many standard deviations a data point is away from the mean.
    Interquartile range (IQR): The IQR is the range between the first quartile (Q1) and the third quartile (Q3) of a distribution.
    Modified z-scores: similar to z-scores, but modified z-scores use the median and a measure called Median Absolute Deviation (MAD) to find outliers. Modified z-scores are generally considered more robust.

For multivariate outliers, we generally use machine learning algorithms. Because of their depth and strength, they are able to find intricate patterns in complex datasets:

    Isolation Forest: uses a collection of isolation trees (similar to decision trees) that recursively divide complex datasets until each instance is isolated. The instances that get isolated the quickest are considered outliers.
    Local Outlier Factor (LOF): LOF measures the local density deviation of a sample compared to its neighbors. Points with significantly lower density are chosen as outliers.
    Clustering techniques: techniques such as k-means or hierarchical clustering divide the dataset into groups. Points that don’t belong to any group or are in their own little clusters are considered outliers.
    Angle-based Outlier Detection (ABOD): ABOD measures the angles between individual points. Instances with odd angles can be considered outliers.

Building an Anomaly Detection Model in Python

Like virtually any task, there are many libraries in Python to perform anomaly detection. The best contenders are:

    Python Outlier Detection (PyOD)
    Scikit-learn

Univariate anomaly detection

import pandas as pd
import seaborn as sns
from pyod.models.mad import MAD

# Load a sample dataset
diamonds = sns.load_dataset("diamonds")
# Extract the feature we want
X = diamonds[["price"]]

# Initialize and fit a model
mad = MAD().fit(X)

# Extract the outlier labels
labels = mad.labels_

>>> pd.Series(labels).value_counts() #0    49708 & 1     4232

outlier_free = diamonds[labels == 0]

>>> len(outlier_free) #49708

labels == 0 creates an array of True/False values (boolean array) where True denotes an inlier.

Multivariate anomaly detection

The process of creating a multivariate anomaly detection model is also the same. But multivariate outlier detection requires extra processing steps if categorical features are present:

>>> diamonds.info()

Since pyod expects all features to be numeric, we need to encode categorical variables. We will use Sklearn:

from sklearn.preprocessing import OrdinalEncoder

# Initialize the encoder
oe = OrdinalEncoder()

# Extract the categorical feature names
cats = diamonds.select_dtypes(include="category").columns.tolist()

# Encode the categorical features
cats_encoded = oe.fit_transform(diamonds[cats])

# Replace the old values with encoded values
diamonds.loc[:, cats] = cats_encoded

>>> diamonds.head()

Before fitting a multivariate model, we will extract the feature array X. The purpose of the diamonds dataset is to predict diamond prices given its characteristics. So, X will contain all columns but price:

X = diamonds.drop("price", axis=1)
y = diamonds[["price"]]
#Now, let’s build and fit the model:
from pyod.models.iforest import IForest

# Create a model with 10000 trees
#The more trees IForest estimator has, the more time it takes to fit the model to the dataset.
iforest = IForest(n_estimators=10000)
iforest.fit(X)  # This will take a minute

# Extract the labels
labels = iforest.labels_

After we have labels, we can remove the outliers from the original data:

X_outlier_free = X[labels == 0]
y_outlier_free = X[labels == 0]

>>> len(X_outlier_free) #48546

>>> # The length of the original dataset
>>> len(diamonds) #53940

The model found over 5000 outliers!
